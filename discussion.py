print("Hello World!")

# [SECTION] Variables 

age = 29
middle_initial = "D"

name1 = "Earl"
name2 = "Ginber"
name3 = "Sherwin"


# Python allows assigning values to multiple variables within a single line
name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

# [SECTION] Data types
# 1. Strings - for alphanumeric characters and symbols
full_name = "John Doe"
secrete_code = "Pa$word"

# 2. Numbers - for int, decimal, and complex numbers
number_of_days = 365 #integer
pi_approximation = 3.1416 #decimal
complex_num = 1 + 5j #complex numbers, represents imaginary component

# 3. Boolean - true or false values
isLearning = True
isDifficult = False

# [SECTION] Using the variables
print("My name is" + full_name)
print("My age is" + ' ' + str(age)) # Typecasting parses data to convert it to different data type

print(int(3.5))#Converts float/decimal value into an integer
print(float(5))# COnverts integer into a float/decimal value

# F-strings
# another way to concatenate and it ignores the strict typing needed w/ regular concatenation using '+' operator
print(f"Hi! My name is {full_name} and my age is {age} ") 

# [SECTION] Operations
# Arithmetic Operators - for mathematical operations
print(1 + 10) #addition
print(15 - 8) #subtraction
print(18 * 9) #Multi
print(21 / 7) #division
print(18 % 4) #Modulo (gets the remainder of division)
print(2 ** 6) # Exponent

# Assignment Operators - for assigning values to variables
num1 = 3
num1 += 4
print(num1)

# Comparison Operators
print(1 == 1)

# Logical Operator
print(True and False)
print(not False)
print(False or True)


