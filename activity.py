name = "Greg"
age = 30
occupation = "developer"
movie = "Catch me if you can"
percent = 99.9


print(f"I am {name}, and I am {age} years old, I work as {occupation}, and my rating for {movie} is {percent} % ")

num1 = 10
num2 = 25
num3 = 15

num1 *= num2
print(num1)
print(num1 <= num3)

num3 += num2
print(num3)
